#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cmath>

template<class type>
void BubbleSort(type* A, int len)
{ 
	type temp;
	bool swapped=true;
  while(len>1 && swapped)
  {
	  swapped=false;
    for(int i=0; i<len-1; i++)
    {
	    if(A[i]>A[i+1])
      {
	      temp=A[i+1];
        A[i+1]=A[i];
        A[i]=temp;
        swapped=1;
      }
    }
    len=len-1;
  }
}

int main()
{
	int len = 10;
	int* tab = new int[len];
  for(int i=0;i<len;i++)
    std::cout << (tab[i]=rand()%len) << '\n'; 
  BubbleSort(tab, len);
  std::cout << '\n';
  for(int i=1;i<len;i++)
    std::cout << tab[i] << '\n'; 
  system("pause");
}

