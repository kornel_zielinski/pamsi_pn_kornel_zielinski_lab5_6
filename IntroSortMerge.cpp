#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cmath>
#include <windows.h>

template<class type>
void Swap(type* tab, int a, int b) //Zamienia element o indeksie a z elementem b
{
  type tmp=tab[a];
  tab[a]=tab[b];
  tab[b]=tmp;   
}

template<class type>              //Obraca tablic� o dlugosci len
void SwapTab(type* tab, int len)
{
  for(int i=0;i<len/2;i++)
    Swap(tab, tab[i], tab[len-i-1]);
}

template<class type>      
bool IsSorted(type* tab, int len)
{
  for(int i=0; i<len-1; i++)
    if(tab[i]>tab[i+1])
      return false;
  return true;
}

template<class type>
int QuickSplit(type* tab, int b, int e)
{
  int ind = (b+e)/2;
  type pivot=tab[ind];
  int pos = b; 
  Swap(tab, ind, e);
  for(int i=b; i < e; i++)
  {
    if(tab[i] < pivot)
    {
      Swap(tab, i, pos);
      pos++;        
    }      
  }
  Swap(tab, pos, e);
  return pos;    
}

template<class type>
void QuickSort(type* tab, int b, int e)
{
  if(b<e)
    {
         /*
      int ind = (b+e)/2;
      type pivot=tab[ind];
      int pos = b; 
      Swap(tab, ind, e);
      for(int i=b; i < e; i++)
      {
        if(tab[i] < pivot)
        {
          Swap(tab, i, pos);
          pos++;        
        }      
      }
      Swap(tab, pos, e);*/
      int pos=QuickSplit(tab, b, e);
      QuickSort(tab, b, pos-1);
      QuickSort(tab, pos+1, e) ;    
    }              
}

template<class type>
void Merge(type* tab, int b, int q, int e, type* tmp)
{
  int i = b;
  int j = q+1;
  for(int k=b; k < e; k++)
  {
    if((tab[i] <= tab[j] || j > e) && i <= q)
    {
	  tmp[k]=tab[i];
      i++;
    }
    else
    {
	  tmp[k]=tab[j];
      j++;
    }
  }
}

//[b]eginning, [e]nd
template<class type>
void MergeSort(type* tab, int b, int e, type* tmp)
{
  if(e-b >= 2)
  {
    int q=(b+e)/2;
    MergeSort(tab, b, q-1, tmp);
    MergeSort(tab, q, e,   tmp);
    Merge(tab, b,  q, e,   tmp);
    for(int i=b;i<e;i++)
      tab[i]=tmp[i];
  }
}

template<class type>
void IntroSort(type* tab, int b, int e, int maxdepth, type* tmp)
{
  int pos;
  if(b>=e)
    return;
  else if(maxdepth=0)
    MergeSort(tab, b, e, tmp);    
  else
    pos=QuickSplit(tab, b, e);
    IntroSort(tab, b, pos-1, maxdepth-1, tmp);
    IntroSort(tab, pos+1, e, maxdepth-1, tmp);
}

LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}

LARGE_INTEGER endTimer()
{
  LARGE_INTEGER stop;
  DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
  QueryPerformanceCounter(&stop);
  SetThreadAffinityMask(GetCurrentThread(), oldmask);
  return stop;
}

int main()
{
  int size[] = {10000, 50000, 100000, 500000, 1000000};
  float percentage[] = {0, 0.25, 0.5, 0.75, 0.95, 0.99, 0.997};
  int* tab;
  int* tmp;
  
  for(int i=7; i<7; i++)   //procent posortowane
  {
    for(int j=0; j<5; j++)   //kazdy rozmiar 
    {
      LARGE_INTEGER start, end;
      int sum=0;
      for(int k=0; k<100; k++) //100 kazdego zestawu
      {
        srand(time(NULL));
        tab = new int[size[j]];
        tmp = new int[size[j]];
        for(int l=0; l<size[j]; l++)
          tab[l] = rand()%size[j];
        IntroSort(tab, 0, (int)(percentage[i]*size[j])-1, (int)(2*log(size[j])), tmp);
        start=startTimer();
        IntroSort(tab, 0, size[j]-1, (int)(2*log(size[j])), tmp);

        end=endTimer();

        if(!IsSorted(tab, size[j]))
          std::cerr << "Tablica nie zosta�a poprawnie posortowana!\n";
        delete[] tab;
        delete[] tmp;
        sum=sum+(end.QuadPart-start.QuadPart);
      }   
      std::cout << "Rozmiar: " << size[j] << ' ' << ", posortowanych: "        
                << percentage[i]*100 << "%\n" << sum/100 << "\n\n";    
    }     
  }

  for(int j=0; j<5; j++)   //kazdy rozmiar 
    {
      LARGE_INTEGER start, end;
      int sum=0;
      for(int k=0; k<100; k++) //100 kazdego zestawu
      {
        srand(time(NULL));
        tab = new int[size[j]];
        for(int l=0; l<size[j]; l++)
          tab[l] = rand()%size[j];
        IntroSort(tab, 0, size[j]-1, (int)(2*log(size[j])), tmp);
        SwapTab(tab, size[j]);
        start=startTimer();
        IntroSort(tab, 0, size[j]-1, (int)(2*log(size[j])), tmp);
        end=endTimer();
        if(!IsSorted(tab, size[j]))
          std::cerr << "Tablica nie zosta�a poprawnie posortowana!\n";
        delete[] tab;
        delete[] tmp;
        sum=sum+(end.QuadPart-start.QuadPart);
      }   
      std::cout << "Rozmiar: " << size[j] << ' ' << ", posortowanych od tylu: "        
                << 100 << "%\n" << sum/100 << "\n\n";
    }
  std::cout << "Done.";
  std::cin >> size[1];  
}
