#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cmath>
#include <windows.h>

template<class type>
void Swap(type* tab, int a, int b) //Zamienia element o indeksie a z elementem b
{
    if(a!=b)
    {
        type tmp=tab[a];
        tab[a]=tab[b];
        tab[b]=tmp;   
    }
}

template<class type>
void SwapTab(type* tab, int len)
{
  for(int i=0;i<len/2;i++)
    Swap(tab, tab[i], tab[len-i-1]);
}

template<class type>
bool IsSorted(type* tab, int len)
{
  for(int i=0; i<len-1; i++)
    if(tab[i]>tab[i+1])
      return false;
  return true;
}

LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}

LARGE_INTEGER endTimer()
{
  LARGE_INTEGER stop;
  DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
  QueryPerformanceCounter(&stop);
  SetThreadAffinityMask(GetCurrentThread(), oldmask);
  return stop;
}

/********* HEAPSORT *********/

void ShiftRight(int* tab, int b, int i, int e)
{
    int root = b+i;
    int left, right, swap;
    while ((root*2)+1 -b <= e)
    {
        left = (root * 2) + 1 - b;
        right = left + 1;
        swap = root;
        if (tab[swap] < tab[left])
            swap = left;
        if ((right <= e) && (tab[swap] < tab[right]))
            swap = right;
        if (swap != root)
        {
            Swap(tab, root, swap);
            root = swap;
        }
        else
        {
            break;
        }
    }
}
void heapify(int* tab, int b, int e)
{
    int i = (e-b-1)/2;
    while (i >= 0)
    {
        ShiftRight(tab, b, i, e);
        --i;
    }
}
void HeapSort(int* tab, int b, int e) //indeksy, w wywolaniu e = size-1
{
    heapify(tab, b, e);
    while (e > b)
    {
        Swap(tab, e, b);
        e--;
        ShiftRight(tab, b, 0, e);
    }
}

/********* INTROSORT *********/

template<class type>
int QuickSplit(type* tab, int b, int e)
{
  int ind = (b+e)/2;
  type pivot=tab[ind];
  int pos = b; 
  Swap(tab, ind, e);
  for(int i=b; i < e; i++)
  {
    if(tab[i] < pivot)
    {
      Swap(tab, i, pos);
      pos++;        
    }      
  }
  Swap(tab, pos, e);
  return pos;    
}

template<class type>
void IntroSort(type* tab, int b, int e, int maxdepth)
{
    int pos;
    if(b<e)
    {
        if(maxdepth==0)
            HeapSort(tab, b, e); 
        else
        {
            pos=QuickSplit(tab, b, e);
            IntroSort(tab, b, pos-1, maxdepth-1);
            IntroSort(tab, pos+1, e, maxdepth-1);
        }
    }
}

int main()
{  
  int size[] = {10000, 50000, 100000, 500000, 1000000};
  float percentage[] = {0, 0.25, 0.5, 0.75, 0.95, 0.99, 0.997};
  int* tab;
  
  for(int i=0; i<7; i++)   //procent posortowane
  {
    for(int j=0; j<5; j++)   //kazdy rozmiar 
    {
      LARGE_INTEGER start, end;
      int sum=0;
      tab = new int[size[j]];
      for(int k=0; k<100; k++) //100 kazdego zestawu
      {
        srand(time(NULL));
        for(int l=0; l<size[j]; l++)
          tab[l] = rand()%size[j];
        IntroSort(tab, 0, (int)(percentage[i]*size[j])-1, (int)(2*log(size[j])));
        start=startTimer();
        IntroSort(tab, 0, size[j]-1, (int)(2*log(size[j])));
        end=endTimer();
        if(!IsSorted(tab, size[j]))
          std::cerr << "Tablica nie zostala poprawnie posortowana!\n";
        sum=sum+(end.QuadPart-start.QuadPart);
      }   
      std::cout << "Rozmiar: " << size[j] << ' ' << ", posortowanych: "        
                << percentage[i]*100 << "%\n" << sum/100 << "\n\n";
      delete[] tab;    
    }     
  }
  for(int j=0; j<5; j++)   //kazdy rozmiar 
    {
      LARGE_INTEGER start, end;
      int sum=0;
      tab = new int[size[j]];
      for(int k=0; k<100; k++) //100 kazdego zestawu
      {
        srand(time(NULL));
        for(int l=0; l<size[j]; l++)
          tab[l] = rand()%size[j];
        IntroSort(tab, 0, size[j]-1, (int)(2*log(size[j])));
        SwapTab(tab, size[j]);
        start=startTimer();
        IntroSort(tab, 0, size[j]-1, (int)(2*log(size[j])));
        end=endTimer();
        if(!IsSorted(tab, size[j]))
          std::cerr << "Tablica nie zostala poprawnie posortowana!\n";
        sum=sum+(end.QuadPart-start.QuadPart);
      }   
      std::cout << "Rozmiar: " << size[j] << ' ' << ", posortowanych od tylu: "        
                << 100 << "%\n" << sum/100 << "\n\n";
      delete[] tab;
    }
  std::cout << "Done.";
  std::cin >> size[1];
}
